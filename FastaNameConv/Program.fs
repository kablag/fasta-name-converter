﻿
open System.Text.RegularExpressions
open System.IO
open System
open System.Net
open System.Text
open System.Xml

type fasta(gi:string ,gb:string, oldN:string, seq:string) =
    let mutable suffix:string = ""
    let mutable taxid = "NA"
    let mutable taxn =  "Noname"
    let mutable cn = "Noname"

    member this.Gi = gi
    member this.Gb = gb
    member this.OldName = oldN
    member this.Suffix = suffix
    member this.TaxId = taxid
    member this.TaxName = taxn
    member this.CommonName = cn
    member this.Seq = seq

    member this.SetSuffix suf =
        suffix <- suf
    member this.SetTaxId id = 
        taxid <- id
    member this.SetTaxName name =
        taxn <- name 
    member this.SetCommonName name =
        cn <- name

let toLowerStr (str:string) =
    str.ToLower()
let splitStr (c:char) (str:string) =    
    str.Split([|c|], StringSplitOptions.RemoveEmptyEntries)

type userCommand =
    | Save
    | Quit    
    | Help
    | AddSuffix
    | Print
    | Empty
    | Error

let parseInput input =
        let parse com = match toLowerStr com with    
                        | "s"  | "save"   -> Save
                        | "q"  | "quit"   -> Quit                        
                        | "h"  | "help" | "?" -> Help
                        | "as" | "add suffix" -> AddSuffix
                        | "p" | "print" -> Print                        
                        | _ -> Error
        match List.ofArray <| splitStr ' ' input with
        | [com]     -> parse com, None
        | com::tail -> parse com, Some(tail)
        | []        -> Empty, None        
        

let (|ParseRegex|_|) regex str =
    let m = Regex(regex).Match(str)
    if m.Success
    then Some (List.tail [ for x in m.Groups -> x.Value ])
    else None

let post (url:string) (reqStr:string) =
    let req = HttpWebRequest.Create(url) :?> HttpWebRequest 
    req.ProtocolVersion <- HttpVersion.Version10
    req.Method <- "POST"    
    let postBytes = Encoding.ASCII.GetBytes(reqStr)
    req.ContentType <- "application/x-www-form-urlencoded";
    req.ContentLength <- int64 postBytes.Length    
    let reqStream = req.GetRequestStream() 
    reqStream.Write(postBytes, 0, postBytes.Length);
    reqStream.Close()
    let resp = req.GetResponse() 
    let stream = resp.GetResponseStream() 
    let reader = new StreamReader(stream) 
    let xml = reader.ReadToEnd()
    let doc = new XmlDocument()
    doc.LoadXml xml
    doc

let elink reqStr =
    post "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/elink.fcgi" reqStr
let esummary reqStr =
    post "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi" reqStr

let setTaxIds fastas =    
    let doc = elink <| "dbfrom=nuccore&db=taxonomy&email=konst.blag@gmail.com" + 
                       Array.fold (fun acc (item:fasta) -> acc + "&id=" + item.Gi) "" fastas   
    
    let taxIds = doc.SelectNodes "/eLinkResult/LinkSet/LinkSetDb/Link[1]/Id/text()"
                 |> Seq.cast<XmlNode>
                 |> Seq.map (fun node -> node.Value)                
    let ids = doc.SelectNodes "/eLinkResult/LinkSet/IdList/Id/text()"
                |> Seq.cast<XmlNode>
                |> Seq.map (fun node -> node.Value)   
    Seq.iter2 (fun id taxid -> let f = query {
                                            for fasta in fastas do
                                            where (fasta.Gi=id)
                                            head }
                               f.SetTaxId(taxid)                                   
                                        ) ids taxIds
    
    fastas

let setTaxNames (fastas:fasta[]) =        
    let doc = esummary <| "db=taxonomy&email=konst.blag@gmail.com&id="
                          + Array.fold (fun acc (item:fasta) -> acc + match item.TaxId with
                                                                      | "NA"  -> ""
                                                                      | taxid -> "," + taxid) "" fastas
    let taxIds  = doc.SelectNodes "/eSummaryResult/DocSum/Item[@Name = \"TaxId\"]/text()"
                |> Seq.cast<XmlNode>
                |> Seq.map (fun (node:XmlNode) -> node.Value)                     
    let taxNames = doc.SelectNodes "/eSummaryResult/DocSum/Item[@Name = \"ScientificName\"]/text()"
                |> Seq.cast<XmlNode>
                |> Seq.map (fun (node:XmlNode) -> node.Value)                
    let commonTaxNames = doc.SelectNodes "/eSummaryResult/DocSum"
                        |> Seq.cast<XmlNode>
                        |> Seq.map (fun (node:XmlNode) -> let cn = node.SelectNodes "Item[@Name = \"CommonName\"]/text()"
                                                                   |> Seq.cast<XmlNode>
                                                                   |> Seq.map (fun (n:XmlNode) -> n.Value)                                                                   
                                                          match Seq.isEmpty cn with
                                                          | true -> "Noname"
                                                          | false -> Seq.nth 0 cn )    
    Seq.iter2 (fun taxid taxname -> let f = query {
                                            for fasta in fastas do
                                            where (fasta.TaxId = taxid && fasta.TaxName = "Noname")
                                            head }
                                    f.SetTaxName(taxname)) taxIds taxNames
    Seq.iter2 (fun taxid ctaxname -> let f = query {
                                            for fasta in fastas do
                                            where (fasta.TaxId = taxid && fasta.CommonName = "Noname")
                                            head }
                                     f.SetCommonName(ctaxname)) taxIds commonTaxNames
    fastas

let genShortName (f:fasta) =
    let genTn (name:string) =
        let elements = List.ofArray <| splitStr ' ' name        
        match elements with
        | [n] -> n
        | head::tail -> 
            List.map (fun (ti:string) ->                                 
                                    match ti.ToCharArray() with
                                    | [||] -> ""
                                    | [|a; b; c; d|] -> ti.[0..3]
                                    | [|a; b; c|] -> ti.[0..2]
                                    | [|a; b|] -> ti.[0..1]
                                    | [|a|] -> sprintf "%c" ti.[0]
                                    | _ -> ti.[0..3]
                                    ) tail
            |> List.reduce (fun acc item -> acc + "." + item) 
            |> sprintf "%c.%s" head.[0] 
        | [] -> "Noname" 
       
    match f.TaxName, f.CommonName with
    | "Noname", _  -> f.OldName
    | tn, "Noname" -> sprintf ">%s_%s_%s%s" (genTn tn) f.TaxId f.Gb f.Suffix
    | tn, cn       -> sprintf ">%s(%s)_%s_%s%s" (genTn tn) cn f.TaxId f.Gb f.Suffix     
     

let parseFasta str =
    match str with
    | ParseRegex "gi\|(\w*)\|\w*\|(\w*)\.?\d?\|\s?\w*\s?.*" [gi; gb] ->                                            
                                            gi , gb
    | _ -> "", ""

let cprintf c fmt =
    Printf.kprintf
        (fun s ->
            let orig = System.Console.ForegroundColor
            System.Console.ForegroundColor <- c;
            System.Console.Write(s)
            System.Console.ForegroundColor <- orig)
        fmt

let cprintfn c fmt =
    Printf.kprintf
        (fun s ->
            let orig = System.Console.ForegroundColor
            System.Console.ForegroundColor <- c;
            System.Console.WriteLine(s)
            System.Console.ForegroundColor <- orig)
        fmt

let printFastasCol fastas =
        Array.iteri (fun i (x:fasta) -> cprintfn ConsoleColor.Cyan "%i:" (i+1)
                                        cprintf ConsoleColor.White "Old Name:"
                                        printfn " %s" x.OldName
                                        cprintf ConsoleColor.White "New Name:"
                                        printfn " %s\n" <| genShortName x) fastas

[<EntryPoint>]
let main argv = 
    let SubFastas f t (a:fasta[]) =
        a.[(f - 1)..(t - 1)]
    let fastas = File.ReadAllText(argv.[0])
                 |> splitStr '>'
                 |> Array.map(fun part -> let n = splitStr '\n' part
                                                 |> List.ofArray                                                              
                                          match n with
                                          | [name] -> name, ""
                                          | name::seq -> name, List.reduce (fun acc item -> acc + "\n" + item) seq                                                                  
                                          | [] -> "","") 
                 |> Array.map (fun (name, seq) ->
                                 let gi, gb = parseFasta name
                                 new fasta(gi, gb, ">" + name, seq)  )
                 //|> ar 28 28             
                 |> setTaxIds
                 |> setTaxNames
//                 |> setShortNames (match suffix with
//                                 | "" -> ""
//                                 | suf -> "_" + suf)
    
    printFastasCol fastas
    
        
    cprintfn ConsoleColor.White "\n\n Try '?' for help\n"
    let help = """Short command (full command) [optional arguments] - Description
* s (save) [filename] - Save (or SaveAs if filename typed) fasta file with new names
* q (quit) - Quit"""
        
    let action = fun _ ->
        cprintf ConsoleColor.White ">: "
        Console.ReadLine()
     
    let readlines = Seq.initInfinite (fun _ -> action())
    
    let printFastas =
        Array.fold (fun acc (f:fasta) -> sprintf "%s%s\n%s\n" acc (genShortName f) f.Seq) "" fastas
        
    let run item = match (parseInput item) with
                   | Save, None   -> File.WriteAllText(argv.[0], printFastas)
                                     Some(item)
                   | Save, Some([fname]) -> File.WriteAllText(fname, printFastas)
                                            Some(item)                   
                   | Quit, None    -> Some(item)                                  
                   | Help, None   -> printfn "%s" help
                                     None                   
                   | AddSuffix, Some([suf]) -> Array.iter (fun (f:fasta) -> f.SetSuffix("_" + suf)) fastas                       
                                               None
                   | Print, None          -> printFastasCol fastas
                                             None
                   | Print, Some([f])     -> printFastasCol <| SubFastas (int f) (int f) fastas
                                             None
                   | Print, Some([f; t])  -> printFastasCol <| SubFastas (int f) (int t) fastas
                                             None
                   | Empty, _  -> None
                   | Error, _  | _, _ -> cprintfn ConsoleColor.Red "Unrecognized input: %s\n Try '?' for help" item
                                         None
                   
    Seq.tryPick run readlines |> ignore    
    0
