# FASTA Name Converter
## Description
Converts long sequence names to short and more "readable" form.
It is useful when multiple alignment tools cut long names in _.aln_ format.

For example:
>gi|37542727|gb|AY064556.1|Y064550S07:146-359 Pan troglodytes forkhead/winged-helix transcription factor (FOXP2) gene, exon 7

becomes
>P.trog(chimpanzee)_9598_AY064556

## Workflow
- retrieves _NuccoreSequenceID_ (37542727) and _GenBankSequenceID_ (AY064556)
- gets _TaxonomyID_ by _NCBI elink_ using _NuccoreSequenceID_
- gets _ScientificName_ and _CommonName_ (if available) by _NCBI eSummary_ using _TaxonomyID_
- forms short name by the rule:
	- first letter from first word of _ScientificName_
	- three letters from next words of _ScientificName_
	- _CommonName_ (if available)
	- _TaxonomyID_
	- _GenBankSequenceID_


